interface ItemList {
    label: string;
    description?: string;
    img?: string;
    imgUrl?: string;
    checked?: boolean;
    santaName?: string;
    checkedDate?: Date;
    creationDate?: Date;
}